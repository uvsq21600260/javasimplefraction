public class Fraction
{
    private float numerateur;
    private float denominateur;
    private float result;

    Fraction(float numerateur, float denominateur)
    {
        this.denominateur=denominateur;
        this.numerateur=numerateur;
        result=numerateur/denominateur;
    }

    @Override
    public String toString()
    {
        return numerateur +"/"+ denominateur+" "+"Le resultat est: "+result;
    }
}
